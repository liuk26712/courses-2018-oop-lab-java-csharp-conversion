using System;

namespace Unibo.Oop.Mnk
{
    public class TurnEventArgs : MNKEventArgs
    {
        public TurnEventArgs(IMNKMatch source, int turn, Symbols player, int i, int j) 
            : base(source, turn, player, i, j)
        {

        }
    }
}